package edu.duke.compsci290.albumviewer;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class AlbumActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); //unsure???
        setContentView(R.layout.activity_album); //sets layout
        Intent receivedIntent = this.getIntent();
        String artist = receivedIntent.getStringExtra("artist_name_key"); //grabs correct album
        String album = receivedIntent.getStringExtra("album_name_key"); //grabs correct album
        String[] songs = this.getResources().getStringArray(getApplicationContext().getResources().getIdentifier(album, "array", getApplicationContext().getPackageName())); //grabs correct song Array
        String albumLower = album.toLowerCase().replaceAll("\\W+", "");
        int drawableId = this.getResources().getIdentifier(albumLower, "drawable", this.getPackageName()); //get correct album name
        Drawable albumArtwork = this.getDrawable(drawableId);

        TextView albumName = findViewById(R.id.album_name_text_view);
        TextView artistName = findViewById(R.id.artist_name_text_view);
        ImageView albumImage = findViewById(R.id.album_artwork_image_view);
        albumImage.setImageDrawable(albumArtwork);
        artistName.setText(artist);
        albumName.setText(album);
        Log.d("ALBUM", songs[0]+songs[1]+songs[2]);

        RecyclerView rv = findViewById(R.id.activity_album_recycler_view); // RecyclerView is flexible and doesn't define parameters for a list of items
        rv.setAdapter(new SongAdapter(this, songs)); //populates RecyclerView with TextView songs
        rv.setLayoutManager(new LinearLayoutManager(this));
        // initialize other views



    }

}
