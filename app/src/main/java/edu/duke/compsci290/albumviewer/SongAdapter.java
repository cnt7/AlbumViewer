package edu.duke.compsci290.albumviewer;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by colintaylor on 1/29/18.
 */

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.ViewHolder> {
    private String[] mSongs;
    private Context mContext;

    public SongAdapter(final Context context, String[] songs){ //**Constructor lookup**
        //Context give instance of where it came from
        mSongs = songs;
        mContext = context;
    }

    @Override
    public SongAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = mInflater.inflate(R.layout.song_holder, parent, false);
        final ViewHolder songHolder = new ViewHolder(row);
        return songHolder; //returns the album holder layout created previously
    }

    @Override
    public void onBindViewHolder(SongAdapter.ViewHolder holder, int position) {
        holder.mSongName.setText(mSongs[position]); //sets text as current song
        Log.d("SoNgS",mSongs[position]);
    }

    @Override
    public int getItemCount() { return mSongs.length; //returns the length of the number of songs
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView mSongName;
        LinearLayout mLinearLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.mLinearLayout = itemView.findViewById(R.id.song_holder_linear_layout);
            this.mSongName = itemView.findViewById(R.id.song_holder_text_view);
        }
    }
}
