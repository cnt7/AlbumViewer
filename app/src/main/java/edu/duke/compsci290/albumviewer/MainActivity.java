package edu.duke.compsci290.albumviewer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity { //child extends parents; mainactivity called when opening app
    @Override //
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); //super means class above us (calls parents)
        setContentView(R.layout.activity_main); //resource file is "R"... connects layout to java file
        String[] albums = this.getResources().getStringArray(R.array.album_names); //connects strings.xml to java file
        String[] artists = this.getResources().getStringArray(R.array.artist_names); /* gets artists array */

        RecyclerView rv = findViewById(R.id.activity_main_recycler_view);
        rv.setAdapter(new AlbumAdapter(this, albums, artists));
        rv.setLayoutManager(new LinearLayoutManager(this));
    }
}
