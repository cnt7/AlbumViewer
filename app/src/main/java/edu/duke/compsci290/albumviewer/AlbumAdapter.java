package edu.duke.compsci290.albumviewer;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import static android.content.ContentValues.TAG;

/**
 * Created by colintaylor on 1/28/18.
 */

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder>{ //<> are arraylist. ".something" = nested class

    private String[] mAlbums; //data classes should be private, and methods should be public
    private String[] mArtists;
    private Context mContext;

    public AlbumAdapter(final Context context, String[] albums, String[] artists){ //**Constructor**
        //Context give instance of where it came from
        mAlbums = albums;
        mArtists = artists;
        mContext = context;
    }

    private void openAlbum(String albumName, String artistName) {
        Log.d(TAG, "Opening album " + albumName);
        Intent intent = new Intent(mContext, AlbumActivity.class);
        intent.putExtra("album_name_key", albumName);
        intent.putExtra("artist_name_key", artistName);
        mContext.startActivity(intent);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = mInflater.inflate(R.layout.album_holder, parent, false);
        final ViewHolder albumHolder = new ViewHolder(row);
        albumHolder.mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAlbum(mAlbums[albumHolder.getAdapterPosition()], mArtists[albumHolder.getAdapterPosition()]);
            }
        });
        return albumHolder; //returns the album holder layout created previously
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String albumName = mAlbums[position].toLowerCase().replaceAll("\\W+", ""); //finds and changes album name to lowercase
        int drawableId = mContext.getResources().getIdentifier(albumName, "drawable", mContext.getPackageName()); //get correct album name
        Drawable albumArtwork = mContext.getDrawable(drawableId);
        holder.mImageView.setImageDrawable(albumArtwork); //sets album image
        holder.mAlbumName.setText(mAlbums[position]); //sets current albumName
        holder.mArtistName.setText("By " + mArtists[position]);// sets current artist name

    }

    @Override
    public int getItemCount() {
        return mAlbums.length; //returns the length of the number of albums in
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        LinearLayout mLinearLayout;
        ImageView mImageView;
        TextView mAlbumName;
        TextView mArtistName;

        public ViewHolder(View itemView) {
            super(itemView);
            this.mLinearLayout = itemView.findViewById(R.id.album_holder_linear_layout);
            this.mImageView = itemView.findViewById(R.id.album_artwork_image_view);
            this.mAlbumName = itemView.findViewById(R.id.album_name_text_view);
            this.mArtistName = itemView.findViewById(R.id.artist_name_text_view);
        }
    }

}

