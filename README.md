# Name: Colin Taylor
## NetID: cnt7
Resources used: Android Studio reference guide, Office Hours,  
Consulted with: sjg36, Cathy, Edwin, and Morton at Office Hours on 1/30/18 and 1/31/18. 
Feedback: This assignment was a little rough because of the time constraint. 
The code released the day the assignment was due was not particularly useful, but 
I think it is a good beginning assignment and helped me learn the content. I did
not finish by the first deadline, but I was able to finish on 1/31/18.